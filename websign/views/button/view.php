<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;


use app\components\ButtonWidget;
?>

<section id="advertisement">
    <div class="container">
        <img src="/images/shop/advertisement.jpg" alt="" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">


            <div class="col-sm-12 padding-right">
                <div class="features_items"><!--features_items-->

                    <h2 class="title text-center"><?= $button->name?></h2>

                        <?php if(!empty($events)): ?>
                            <?php  $i=0; foreach ($events as $event): ?>
                                <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">

                                            <?= Html::img("@web/images/events-foto/{$event->img}",['alt'=>$event->name]) ?>

                                            <h2><a href="<?= Url::to(['myevent/view','id'=>$event->id])?>"><?= $event->name ?></a></h2>
                                            <a href="<?= Url::to(['part/add','id'=>$event->id]) ?>" data-id="<?= $event->id ?>"
                                               class="btn btn-default add-to-cart">
                                                <i class="fa fa-shopping-cart"></i>Прийняти участь</a>
                                        </div>

                                    </div>

                                </div>
                            </div>


                            <?php if($i%3==0): ?>
                                <div class="clearfix" ></div>
                            <?php endif; ?>
                        <?php endforeach;?>
                        <div class="clearfix" ></div>
                            <?php
                        echo LinkPager::widget(['pagination' => $pages,])
                                ?>
                    <?php else:?>
                        <h2 id="no-ivent">Івентів немає!!!</h2>
                    <?php endif; ?>



                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>
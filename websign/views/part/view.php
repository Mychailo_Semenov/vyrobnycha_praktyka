<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<section id="cart_items">

    <div class="container">

        <!--флешка-->
        <?php if (Yii::$app->session->hasFlash('success')) :?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><?php echo Yii::$app->session->getFlash('success'); ?></strong>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->session->hasFlash('error')) :?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><?php echo Yii::$app->session->getFlash('error'); ?></strong>
            </div>
        <?php endif; ?>




        <?php if(!empty($session['part'])):?>
        <div class="table-responsive cart_info  ">
            <table class="table table-condensed"><!--table-hover table-striped-->
                <thead>
                <tr class="cart_menu">
                    <td class="image">Фото</td>
                    <td class="description">Івент</td>
                    <td ></td>
                    <td ></td>

                    <td>x</td>
                </tr>
                </thead>
                <tbody>
                <?php  foreach ($session['part'] as $id=>$item):?>
                    <tr>
                        <td class="cart_product">
                            <a href=""> <?php if($item['image']):?>
                                    <?=Html::img($item['image'],
                                        ['alt'=>$item['name'],'height'=>200]) ?>
                                <?php else :?>
                                    <?=Html::img("@web/images/events-foto/{$item['img']}",
                                        ['alt'=>$item['name'],'height'=>200]) ?>
                                <? endif;?>

                            </a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="<?= Url::to(['myevent/view', 'id'=>$id])?>"><?=$item['name'] ?></a></h4>

                        </td>

                        <td class="cart_delete">
                            <a href="#"data-id="<?= $id ?>" class="del_item" ><i class="fa fa-times"></i></a>
                        </td>
                    </tr>

                <?php  endforeach; ?>
                </tbody>
            </table>
            <section id="do_action">
                <div class="container">

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="total_area">
                                <ul>
                                    <li>Всього івентів<span><?= count($session['part']) ?></span></li>

                                </ul>


                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-1 class-login-form">
                            <div class="login-form">
                                <?php $form = ActiveForm::begin() ?>
                                <?= $form->field($eventregister,'name')?>
                                <?= $form->field($eventregister,'email')?>
                                <?= $form->field($eventregister,'phone')?>
                                <?= $form->field($eventregister,'address')?>
                                <?= Html::submitButton('Оформити', ['class'=>'btn btn-default'])?>
                                <?php ActiveForm::end()?>
                            </div><!--/login form-->
                        </div>
                    </div>
                </div>
            </section><!--/#do_action-->
        </div>
    </div>
    <?php else: ?>
        <h3>Івентів немає</h3>
    <?php endif;?>

</section> <!--/#cart_items-->


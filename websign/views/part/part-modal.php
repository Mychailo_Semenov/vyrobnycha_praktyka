<?php use yii\helpers\Html;?>
<section id="cart_items">
    <?php if(!empty($session['part'])):?>
        <div class="table-responsive cart_info  ">
        <table class="table table-condensed"><!--table-hover table-striped-->
        <thead>
        <tr class="cart_menu">
            <td class="image">Фото івенту</td>
            <td></td>
            <td class="description">Подія/івент</td>
            <td ></td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        <?php  foreach ($session['part'] as $id=>$item):?>
            <tr>
                <td class="cart_product">
                    <div>
                        <a href="">
                            <?php if($item['image']):?>
                                <?=Html::img($item['image'],
                                    ['alt'=>$item['name'],'height'=>100]) ?>
                            <?php else :?>
                                <?=Html::img("@web/images/events-foto/{$item['img']}",
                                    ['alt'=>$item['name'],'height'=>100]) ?>
                            <? endif;?>
                        </a>
                    </div>
                </td>
                <td></td>
                <td class="cart_description">
                    <div></div>
                    <h4><a href=""><?=$item['name'] ?></a></h4>

                </td>



            <td class="cart_delete">
                <a data-id="<?= $id ?>" class="del_item" ><i class="fa fa-times del_item"></i></a>
            </td>
            </tr>

        <?php  endforeach; ?>
        </tbody>
        </table>
        <section id="do_action">
            <div class="container">

                <div class="row">

                    <div class="col-sm-6">
                        <div class="total_area">
                            <ul>
                                <li>Всього івентів<span><?= count($session['part']) ?></span></li>

                            </ul>


                        </div>
                    </div>
                </div>
            </div>
        </section><!--/#do_action-->
        </div>
    <?php else: ?>
        <h3>Івентів немає</h3>
    <?php endif;?>

</section> <!--/#cart_items-->


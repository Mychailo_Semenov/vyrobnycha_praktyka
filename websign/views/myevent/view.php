<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<section>
    <div class="container">
        <div class="row">

            <?php
            $mainImg = $event->getImage();
            $gallery = $event->getImages();
            ?>

            <div class="col-sm-12"><!--/Вивід івенту--------------------------------->
                <div class="blog-post-area">
                    <h2 class="title text-center"><?= $event->button->name?></h2>
                    <div class="single-blog-post">
                        <div class="article-2">
                            <a  href="<?= Url::to() ?>" data-shiny-button class="shiny-lable" >
                                <p><?= $event->name?></p>
                                <canvas></canvas>
                            </a>
                        </div>

<!--                        <h3>--><?//= $event->name?><!--</h3>-->
                        <div class="col-sm-4">
                            <a href="" >
                                <?php if($mainImg['urlAlias'] == 'placeHolder') : ?>
                                    <?= Html::img("@web/images/events-foto/{$event->img}",['alt'=>$event->name])?>
                                <? else :?>
                                    <?= Html::img($mainImg->getUrl(),['alt'=>$event->name])?>
                                <? endif;?>
                            </a>
                        </div>
                        <p><?= $event->content?></p> <br>
                        <div class="clearfix"></div>

                        <?php if($gallery[0]['urlAlias'] != 'placeHolder') : ?>

                            <div class="recommended_items"><!--gallery-->
                                <h2 class="title text-center">-</h2>

                                <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <?php $count=count($gallery); $i=0; foreach ($gallery as $item):?>
                                            <?php   if ($i%3==0):?>
                                                <div class="item <?php if($i==0) echo 'active'?>">
                                            <?php  endif;?>
                                            <div class="col-sm-4">
                                                <div class="product-image-wrapper">
                                                    <div class="single-products">
                                                        <div class="productinfo text-center">
                                                            <?= Html::img($item->getUrl('300x300'),['alt'=>''])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $i++; if ($i%3==0||$i==$count):?>
                                                </div>
                                            <?php  endif;?>
                                        <?php endforeach;?>
                                    </div>
                                    <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                    <h2 class="title text-center">.</h2>
                                </div>
                            </div><!--/recommended_items-->
                        <?php endif;?>


                    </div>
                </div>
                    <div class="article">
                        <a  data-id="<?= $event->id ?>" data-shiny-button class="shiny-button add-to-cart" >
                            <p>Реєстрація</p>
                                <canvas></canvas>
                        </a>
                    </div>
            </div>
        </div>
    </div>
</section>

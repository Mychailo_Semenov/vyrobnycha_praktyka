<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_register".
 *
 * @property int $id
 * @property string $created
 * @property string $updated
 * @property int $count_part
 * @property string $status
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $address
 */
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class EventRegister extends ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'event_register';
    }

    public function behaviors(){
        //з документації YII2
        return [
            [
                'class'=>TimestampBehavior::className(),
                'attributes'=>[
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created','updated'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
                'value'=>new Expression('NOW()'),
            ],
        ];
    }

    public function getEventRegisterItems(){
        return $this->hasMany(EventRegisterItems::className(),['event_register_id'=>'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'address'], 'required'],
            [['created', 'updated'], 'safe'],
            [['count_part'], 'integer'],
            [['status'], 'boolean'],
            [['name', 'email', 'phone', 'address'], 'string', 'max' => 250],
            [['email'],'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'name' => 'Ім\'я',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'address' => 'Адреса',
        ];
    }
}

<?php


namespace app\models;
use yii\db\ActiveRecord;

class AllEvent extends ActiveRecord {

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }



    public static function tableName() {
        return 'all_event';
    }
    public function getButton()
    {
        return $this->hasOne(Button::className(), ['id' => 'button_id']);
    }
}
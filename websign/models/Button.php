<?php


namespace app\models;
use yii\db\ActiveRecord;

class Button extends ActiveRecord {


    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }



    public static function tableName(){
        return 'button';
    }
    public function getAllEvents()
    {
        return $this->hasMany(AllEvent::className(), ['button_id' => 'id']);
    }
}
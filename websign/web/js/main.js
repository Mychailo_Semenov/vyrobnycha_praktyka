/*price range*/

{
	const colorStops = [
		[0, "rgb(84, 94, 109)"],
		[0.2, "rgb(111, 122, 137)"],
		[0.4, "rgb(153, 156, 168)"],
		[0.6, "rgb(195, 200, 219)"],
		[0.9, "rgb(74, 73, 83)"],
		[1, "rgb(93, 93, 93)"]
	];

	const buttons = document.querySelectorAll("[data-shiny-button]");
	const createShinyButtons = (...buttons) => {
		const shines = buttons.map(button => {

			const loopPartial = () => {
				const c = button.querySelector("canvas");
				const $ = c.getContext("2d");
				const { width, height } = button.getBoundingClientRect();
				let w = (c.width = width);
				let h = (c.height = height);

				return (x, y, width, height, i) => {
					const xRatio = width / x || 0;
					const yRatio = height / y || 0;
					console.log(i);
					const diff = i + 1;
					const gradient = $.createLinearGradient(
						yRatio * (w / 2) / 8 * -diff * 2,
						xRatio * (w / 2) / 8 - diff * 2,
						xRatio * (w / 2),
						yRatio * h / (diff / 2)
					);

					colorStops.forEach(([stop, color]) =>{
						gradient.addColorStop(stop, color);
					});

					$.fillStyle = gradient;
					$.fillRect(0, 0, w, h);
				};
			};
			const loop = loopPartial();
			const innerWidth = window.innerWidth;
			const innerheight = window.innerHeight;
			// Init
			loop(innerWidth / 2, innerHeight / 2, innerWidth, innerHeight, 0);
			return loop;
		});

		document.addEventListener("mousemove", ({ clientX, clientY }) => {
			shines.forEach((shine, i) => {
				const innerWidth = window.innerWidth;
				const innerHeight = window.innerHeight;
				requestAnimationFrame(() =>
					shine(clientX, clientY, innerWidth, innerHeight, i)
				);
			});
		});
	};
	createShinyButtons(...buttons);
}





 $('#sl2').slider();

	var RGBChange = function() {
	  $('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')')
	};	
		
/*scroll to top*/

$(document).ready(function(){


	function showPart(part){
		$('#partModal .modal-body').html(part);
		$('#partModal').modal();
	}



	$('.add-to-cart').on('click',function (e) {
		e.preventDefault();
		var id = $(this).data('id');
		$.ajax({
			url: '/part/add',
			data: {id: id},
			type: 'GET',
			success: function(res) {
				if (!res) alert('Помилка!');
				// console.log(res);
				showPart(res);

			},
			error: function () {
				alert('Error!')
			}
		});
	});
	function getPart(){
		$.ajax({
			url: '/part/show',
			type: 'GET',
			success: function(res) {
				if (!res) alert('Помилка!');
				showPart(res);

			},
			error: function () {
				alert('Error')

			}
		});
	}






	$('.getPart').on('click',function (){

		$.ajax({
			url: '/part/show',
			type: 'GET',
			success: function(res) {
				if (!res) alert('Помилка!');
				showPart(res);

			},
			error: function () {
				alert('Error')

			}
		});
	});

	$('.del_item').on('click',function (){
		var id = $(this).data('id');
		// console.log(id);
		$.ajax({
			url: '/part/del-item',
			data:{id: id},
			type: 'GET',
			success: function(res) {
				if (!res) alert('Помилка!');
				showPart(res);

			},
			error: function () {
				alert('Error')

			}
		});
	});



	$('#partModal .modal-body').on('click','.del_item', function () {
		var id = $(this).data('id');
		// console.log(id);
		$.ajax({
			url: '/part/del-item',
			data:{id: id},
			type: 'GET',
			success: function(res) {
				if (!res) alert('Помилка!');
				showPart(res);

			},
			error: function () {
				alert('Error')

			}
		});
	});

	$('.clearRegister').on('click',function () {
		$.ajax({
			url: '/part/clear',
			type: 'GET',
			success: function(res) {
				if (!res) alert('Помилка!');
				showPart(res);

			},
			error: function () {
				alert('Error')

			}
		});
	});




	$(function () {
		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});
});

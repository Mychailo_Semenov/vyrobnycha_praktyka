<?php

namespace app\components;
use yii\base\Widget;
use app\models\Button;
use Yii;
class ButtonWidget extends Widget {
    public $tpl;
    public $data;
    public $tree;
    public $menuHtml;
    public $model;

    public function init() {
        parent::init();
        if ($this->tpl ===null){
            $this->tpl = 'menu';
        }
        $this->tpl .= '.php';
    }

    public function run(){
        // get cache
        if ($this->tpl == 'menu.php'){
            $menu = Yii::$app->cache->get('menu');
            if ($menu) return $menu;
        }

        $this->data = Button::find()->indexBy('id')->asArray()->all();
        $this->tree = $this->getTree();
        $this->menuHtml = $this->getMenuHtml($this->tree);
//        debug($this->tree);
//set cache
        if ($this->tpl == 'menu.php'){
            Yii::$app->cache->set('menu', $this->menuHtml, 60);
        }

        return $this->menuHtml;
    }
    //отримаемо дерево з массиву
    public function getTree(){
        $tree = [];
        foreach ($this->data as $id =>&$node){
            if(!$node['parent_id'])
                $tree[$id]=&$node;
            else
                $this->data[$node['parent_id']]['childs'][$node['id']]=&$node;
        }
        return $tree;
    }
    //приймає дерево як параметр і передає методу catToTemplate і вертає таким чином Html код
    // $tab--->параметр для створення отступу в селекті адмін панелі категорій
    protected function getMenuHtml($tree, $tab =''){
        $str = '';
        foreach ($tree as $button) {
            $str .= $this->catToTemplate($button, $tab);
        }
        return $str;
    }
    //вставляє кожний елемент в шаблон, буферизуючи вивід і не виводячи на екран
    protected function catToTemplate($button, $tab){
        ob_start();
        include __DIR__ . '/menu_tpl/' . $this->tpl;
        return ob_get_clean();
    }

}
<?php

namespace app\controllers;
use app\models\AllEvent;
use app\models\Part;
use Yii;
use app\models\EventRegister;
use app\models\EventRegisterItems;
use yii\db\Exception;


class PartController extends AppController {
    public function actionAdd(){
        $id = Yii::$app->request->get('id');
//        debug($id);

        $event =ALLEvent::findOne($id);
        if(empty($event))return false;
//        debug($event);
        $session=Yii::$app->session;
        $session->open();
        $part =new Part();
        $part->addToPart($event);
//        debug($session['part']);

        //Якщо запит не Ajax
        if(!Yii::$app->request->isAjax ){
            //вертаємось на туж сторінку
            return $this->redirect(Yii::$app->request->referrer);
        }
//
        //прибераумо шаблон
        $this->layout=false;
        return $this->render('part-modal',compact('session'));
    }
    public function actionClear(){
        $session=Yii::$app->session;
        $session->open();
        $session->remove('part');

        $this->layout=false;
        return $this->render('part-modal',compact('session'));
    }
    public function actionDelItem(){
        $id = Yii::$app->request->get('id');
        $session=Yii::$app->session;
        $session->open();
        $part = new Part();
        $part->rewrite($id);
        $this->layout=false;
        return $this->render('part-modal',compact('session'));
    }
    public function actionShow(){
        $session=Yii::$app->session;
        $session->open();
        $this->layout=false;
        return $this->render('part-modal',compact('session'));
    }
    public function actionView(){
        $session=Yii::$app->session;
        $session->open();
        $this->setMeta("SOFT | Реєстрація");
        $eventregister = new EventRegister();
        //приймаємо дані
        if ($eventregister->load(Yii::$app->request->post())){
//            debug(Yii::$app->request->post());
            //заносимо дані в таблицю

            $eventregister->count_part = count($session['part']);
            if($eventregister->save()) {
                $this->saveEventRegisterItems($session['part'], $eventregister->id);
///////////////////////////////////////////////////////////////////////
                $number =$eventregister->id;
//debug($session );


                //flash повідомлення
                Yii::$app->session->setFlash('success', 'Ваша заявка прийнята,
                     номер реєстрації :' . "<h1>$number</h1>");


//                // відправляємо листа
                Yii::$app->mailer->compose('layouts/register', ['session' => $session,'number'=>$number])
                    ->setFrom(['semyonov.mychayl@gmail.com' => 'SOFT'])
                    ->setTo($eventregister->email)
                    ->setSubject('Peєстрація на сервісі SOFT')
                    ->send();
                $session->remove('part');

                return $this->refresh();

            }else{
                Yii::$app->session->setFlash('error','Помилка оформлення заявки на участь');
            }
        }
        return $this->render('view', compact('session','eventregister'));
    }
    protected function saveEventRegisterItems($items, $eventregister_id){
        foreach ($items as $id=>$item){
            $eventeregister_items = new EventRegisterItems();
            $eventeregister_items->event_register_id = $eventregister_id;
            $eventeregister_items->event_id = $id;
            $eventeregister_items->name = $item['name'];

            $eventeregister_items->save();

        }
    }
}
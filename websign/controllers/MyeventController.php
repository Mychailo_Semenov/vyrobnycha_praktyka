<?php


namespace app\controllers;
use app\models\Button;
use app\models\AllEvent;
use Yii;
use yii\web\HttpException;

class MyeventController extends AppController {
    public function actionView($id){
        //наш івент або з параметру public function actionView($id)
//        $id =Yii::$app->request->get('id');
        //лінива
        $event = AllEvent::findOne($id);
        //жадібна
//        $event = MyEvent::find()->with('category')->where(['id'=>$id])->limit(1)->one();
        //----------------------
        if(empty($event))
            throw new HttpException(404,'Такого івенту немає');
        //----------------------
        //відкриті заходи
//        $opens = AllEvent::find()->where(['open'=>'1'])->limit(6)->all();
        //meta
        $this->setMeta('SOFT | '. $event->name,$event->keywords,
            $event->description);

        return $this->render('view',compact('event'));


    }

}
<?php


namespace app\controllers;

use app\models\Button;
use app\models\AllEvent;
use Yii;
use yii\data\Pagination;


class ButtonController extends AppController {
    public function actionIndex() {
        $this->setMeta('SOFT');
        return $this->render('index');
    }
    public function actionView($id){
        $id = Yii::$app->request->get('id');

//        $events = AllEvent::find()->where(['button_id'=>$id])->all();

        //pagination
        $query =AllEvent::find()->where(['button_id'=>$id]);
        $pages = new Pagination(['totalCount'=>$query->count(),'pageSize' => 3,
            //додаткові параметри для відображення адреси в браузері
            'forcePageParam' => false,'pageSizeParam' => false]);
        $events = $query->offset($pages->offset)->limit($pages->limit)->all();

        $button = Button::findOne($id);

        $this->setMeta('SOFT | ' . $button->name,$button->keywords, $button->description);

        return $this->render('view', compact('events','button','pages'));

    }

}
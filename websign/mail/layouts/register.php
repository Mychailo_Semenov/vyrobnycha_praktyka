<div class="table-responsive">
    <table style="width: 100%; border: 1px solid #ddd; border-collapse: collapse;">
        <thead>
        <tr style="background: #f9f9f9;">
            <td style="padding: 8px; border: 1px solid #ddd;">Івент</td>
            <td style="padding: 8px; border: 1px solid #ddd;">Номер реєстрації</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($session['part'] as $id => $item): ?>
            <tr>
                <td style="padding: 8px; border: 1px solid #ddd;">
                    <h4><a href=""><?= $item['name'] ?></a></h4>
                </td>
                <td style="padding: 8px; border: 1px solid #ddd;">
                    <h4><a href=""><?= $number ?></a></h4>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <section id="do_action">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="total_area">
                        <ul>
                            <li style="padding: 8px; border: 1px solid #ddd;">Всього івентів:
                                <span><?= count($session['part']) ?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>




<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EventRegister */

$this->title = '№'. " $model->id";
$this->params['breadcrumbs'][] = ['label' => 'Event Registers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="register-view">

    <h1>Перегляд реєстрації  № <?= $model->id?></h1>

    <p>
        <?= Html::a('Змінити', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created',
            'updated',
            'count_part',
//            'status',
            [
                'attribute'=>'status',
                'value'=> function($dat){
                    return !$dat->status ? '<span class="text-danger">Активний</span>' :
                        '<span class="text-success">Завершений</span>' ;
                },
                'format'=>'html',
            ],
            'name',
            'email:email',
            'phone',
            'address',
        ],
    ]) ?>
    <?php $items =$model->eventRegisterItems; ?>
    <div class="table-responsive ">
        <table class="table table-hover table-striped">
            <div class="table-responsive cart_info  ">
                <table class="table table-condensed"><!--table-hover table-striped-->
                    <thead>
                    <tr class="cart_menu">
                        <td class="description"></td>
                        <td class="price">Тема реєстрації</td>
                        <td class="price">Номер клієнта</td>
                        <td class="quantity">Номер реєстрації</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php  foreach ($items as $item):?>
                        <tr>

                            <td class="cart_description">
                                <h4><a href="<?= Url::to(['/myevent/view', 'id'=>$item['id']])?>"><?=$item['name'] ?></a></h4>

                            </td>
                            <td class="cart_price">
                                <p><?=$item['name'] ?></p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <span><?=$item['event_register_id'] ?></span>
                                </div>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <span><?=$item['id'] ?></span>
                                </div>
                            </td>

                        </tr>

                    <?php  endforeach; ?>
                    </tbody>
                </table>

            </div>
        </table>
    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AllEvent */

$this->title = 'Створити івент';
$this->params['breadcrumbs'][] = ['label' => 'All Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="myevent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Івенти';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="myevent-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Створити івент', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'button_id',
            [
                'attribute'=>'button_id',
                'value' => function($dat){
                    return $dat->button->name;

                },
            ],
            'name',
//            'content:ntext',
            'description',
            //'keywords',
            //'img',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

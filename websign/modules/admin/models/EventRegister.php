<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "event_register".
 *
 * @property int $id
 * @property string $created
 * @property string $updated
 * @property int $count_part
 * @property string $status
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $address
 */
class EventRegister extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_register';
    }
    public function getEventRegisterItems(){
        return $this->hasMany(EventRegisterItems::className(),['event_register_id'=>'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created', 'updated', 'count_part', 'name', 'email', 'phone', 'address'], 'required'],
            [['created', 'updated'], 'safe'],
            [['count_part'], 'integer'],
            [['status'], 'string'],
            [['name', 'email', 'phone', 'address'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ реєстрації',
            'created' => 'Дата створення',
            'updated' => 'Дата оновлення',
            'count_part' => 'Кількість івентів',
            'status' => 'Статус',
            'name' => 'Ім\'я',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'address' => 'Адресса',
        ];
    }
}

<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "all_event".
 *
 * @property int $id
 * @property int $button_id
 * @property string $name
 * @property string|null $content
 * @property string|null $description
 * @property string|null $keywords
 * @property string|null $img
 */
class AllEvent extends \yii\db\ActiveRecord {

    public $image;
    public $gallery;

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'all_event';
    }
    public function getButton(){
        return $this->hasOne(Button::className(),['id'=>'button_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['button_id', 'name'], 'required'],
            [['button_id'], 'integer'],
            [['content'], 'string'],
            [['name', 'description', 'keywords', 'img'], 'string', 'max' => 250],
            [['image'], 'file', 'extensions' => 'png, jpg'],
            [['gallery'], 'file', 'extensions' => 'png, jpg', 'maxFiles'=> 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ реєстрації',
            'button_id' => 'Категорія/кнопка',
            'name' => 'Назва',
            'content' => 'Контент',
            'description' => 'Мета-опис',
            'keywords' => 'Ключові слова',
            'image' => 'Фото',
            'gallery' =>'Галерея',
        ];
    }

    public function upload(){
        if($this->validate()){
            $path = 'upload/store/' . $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs($path);
            $this->attachImage($path, true);
            @unlink($path);
            return true;
        }else{
            return false;
        }
    }

    public function uploadGallery(){
        if($this->validate()){
            foreach($this->gallery as $file){
                $path = 'upload/store/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                $this->attachImage($path);
                @unlink($path);
            }

            return true;
        }else{
            return false;
        }
    }



}

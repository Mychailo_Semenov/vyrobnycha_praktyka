<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "button".
 *
 * @property int $id
 * @property string $name
 * @property string|null $keywords
 * @property string|null $description
 */
class Button extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'button';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'keywords', 'description'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ категорії/кнопки',
            'name' => 'Ім\'я',
            'keywords' => 'Ключові слова',
            'description' => 'Мета-опис',
        ];
    }
}

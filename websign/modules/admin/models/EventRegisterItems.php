<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "event_register_items".
 *
 * @property int $id
 * @property int $event_register_id
 * @property int $event_id
 * @property string $name
 */
class EventRegisterItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_register_items';
    }

    public function getEventRegister(){
        return $this->hasOne(EventRegister::className(),['id'=>'event_register_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_register_id', 'event_id', 'name'], 'required'],
            [['event_register_id', 'event_id'], 'integer'],
            [['name'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_register_id' => 'Event Register ID',
            'event_id' => 'Event ID',
            'name' => 'Name',
        ];
    }
}
